package cvir2018MV;

import cvir2018MV.control.CarteCtrl;
import cvir2018MV.model.Carte;
import cvir2018MV.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class IntegrationTestTD {
    private CarteCtrl ctrl;
    private CartiRepoMock ci;

    @Before
    public void init(){
        ci = new CartiRepoMock();//
        ctrl = new CarteCtrl(ci);
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test1() throws Exception {
        Carte c = new Carte();
        List<String> autori = new ArrayList<String>();
        autori.add( "Mateiu Caragiale" );

        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add( "mateiu" );
        cuvinteCheie.add( "crailor" );

        c.setTitlu( "Intampinarea crailor" );
        c.setReferenti( autori );
        c.setAnAparitie( "1948" );
        c.setEditura( "Litera" );
        c.setCuvinteCheie( cuvinteCheie );

        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            throw e;
        }
        assertEquals(8, ctrl.getCarti().size());
        assertEquals("Intampinarea crailor", c.getTitlu());
        assertEquals(autori,c.getReferenti());
        assertEquals( "1948",c.getAnAparitie() );
        assertEquals("Litera", c.getEditura() );
        assertEquals(cuvinteCheie, c.getCuvinteCheie()  );
    }

    @Test
    public void test2() throws Exception {
        //P->B-A
        List<Carte> ca = ctrl.cautaCarte( "mada" );
        for (Carte i :ca
                ) {
            List<String> lref = i.getReferenti();
            assertEquals( lref.size(),0);
        }
        Carte c = new Carte();
        List<String> autori = new ArrayList<String>();
        autori.add( "Pop Ionescu" );

        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add( "mateiu" );
        cuvinteCheie.add( "crailor" );

        c.setTitlu( "Istoria Informaticii" );
        c.setReferenti( autori );
        c.setAnAparitie( "1998" );
        c.setEditura( "Bucovina" );
        c.setCuvinteCheie( cuvinteCheie );

        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            throw e;
        }
        assertEquals(8, ctrl.getCarti().size());
        assertEquals("Istoria Informaticii", c.getTitlu());
        assertEquals(autori,c.getReferenti());
        assertEquals( "1998",c.getAnAparitie() );
        assertEquals("Bucovina", c.getEditura() );
        assertEquals(cuvinteCheie, c.getCuvinteCheie()  );
    }

    @Test
    public void test3() throws Exception {
        //P->C->B->A B-invalid A-valid C-invalid
        List<Carte> ca = ctrl.getCartiOrdonateDinAnul( "1949" );
        assertEquals( ca.size(),0 );

        List<Carte> car = ctrl.cautaCarte( "mada" );
        for (Carte i :car
                ) {
            List<String> lref = i.getReferenti();
            assertEquals( lref.size(),0);
        }

        Carte c = new Carte();
        List<String> autori = new ArrayList<String>();
        autori.add( "Irina Binder" );

        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add( "fluturi" );
        cuvinteCheie.add( "iubire" );

        c.setTitlu( "Fluturi" );
        c.setReferenti( autori );
        c.setAnAparitie( "2001" );
        c.setEditura( "Elf" );
        c.setCuvinteCheie( cuvinteCheie );

        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            throw e;
        }
        assertEquals(8, ctrl.getCarti().size());
        assertEquals("Fluturi", c.getTitlu());
        assertEquals(autori,c.getReferenti());
        assertEquals( "2001",c.getAnAparitie() );
        assertEquals("Elf", c.getEditura() );
        assertEquals(cuvinteCheie, c.getCuvinteCheie()  );

    }
}