package cvir2018MV;

import cvir2018MV.control.CarteCtrl;
import cvir2018MV.model.Carte;
import cvir2018MV.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class CautareTest {
    private CarteCtrl ctrl;
    private CartiRepoMock ci;

    @Before
    public void init() {
        ci = new CartiRepoMock();//
        ctrl = new CarteCtrl( ci );
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();


    @Test
    public void test1WBT() throws Exception {

		List<Carte> ca = ctrl.cautaCarte( "ion" );
        for (Carte i :ca
             ) {
            List<String> lref = i.getReferenti();
          for (int j=0;j<lref.size();j++) {
              if(lref.get( j ).contains( "ion".toLowerCase() )){
                  assertEquals( lref.get( j ).contains( "ion".toLowerCase()) ,true );
              }
          }
        }
    }

    @Test
    public void test2NWBT() throws Exception {

        List<Carte> ca = ctrl.cautaCarte( "mada" );
        for (Carte i :ca
                ) {
            List<String> lref = i.getReferenti();
                    assertEquals( lref.size(),0);
        }
    }



    @Test
    public void test3() throws Exception {

        List<Carte> ca = ctrl.cautaCarte( "george" );
        for (Carte i :ca
                ) {
            List<String> lref = i.getReferenti();
            for (int j=0;j<lref.size();j++) {
                if(lref.get( j ).contains( "george".toLowerCase() )){
                    assertEquals( lref.get( j ).contains( "george".toLowerCase()) ,true );
                }
            }
        }
    }
}