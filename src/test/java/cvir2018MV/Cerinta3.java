package cvir2018MV;

import cvir2018MV.control.CarteCtrl;
import cvir2018MV.model.Carte;
import cvir2018MV.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Cerinta3 {
    private CarteCtrl ctrl;
    private CartiRepoMock ci;

    @Before
    public void init() {
        ci = new CartiRepoMock();//
        ctrl = new CarteCtrl( ci );
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();


    @Test
    public void test1() throws Exception {

        List<Carte> ca = ctrl.getCartiOrdonateDinAnul( "1948" );
        assertEquals( ca.size(),3 );
        List<Carte> carti = new ArrayList<Carte>();

        carti.add(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        carti.add(Carte.getCarteFromString("Enigma Otiliei;George Calinescu;1948;Litera;enigma,otilia"));
        carti.add(Carte.getCarteFromString("Intampinarea crailor;Mateiu Caragiale;1948;Litera;mateiu,crailor"));

//            for (int j=0;j<ca.size();j++) {
//                    assertEquals(ca.get(j),carti.get(j));
//
//        }
    }

    @Test
    public void test2() throws Exception {

        List<Carte> ca = ctrl.getCartiOrdonateDinAnul( "1949" );
        assertEquals( ca.size(),0 );
        }

}