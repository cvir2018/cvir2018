package cvir2018MV;

import cvir2018MV.control.CarteCtrl;
import cvir2018MV.model.Carte;
import cvir2018MV.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class IntegrationTestBB {
    private CarteCtrl ctrl;
    private CartiRepoMock ci;

    @Before
    public void init(){
        ci = new CartiRepoMock();//
        ctrl = new CarteCtrl(ci);
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test1() throws Exception {
        Carte c = new Carte();
        List<String> autori = new ArrayList<String>();
        autori.add( "Mateiu Caragiale" );

        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add( "mateiu" );
        cuvinteCheie.add( "crailor" );

        c.setTitlu( "Intampinarea crailor" );
        c.setReferenti( autori );
        c.setAnAparitie( "1948" );
        c.setEditura( "Litera" );
        c.setCuvinteCheie( cuvinteCheie );

        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            throw e;
        }
        assertEquals(8, ctrl.getCarti().size());
        assertEquals("Intampinarea crailor", c.getTitlu());
        assertEquals(autori,c.getReferenti());
        assertEquals( "1948",c.getAnAparitie() );
        assertEquals("Litera", c.getEditura() );
        assertEquals(cuvinteCheie, c.getCuvinteCheie()  );
    }

    @Test
    public void test2() throws Exception {

        List<Carte> ca = ctrl.cautaCarte( "mada" );
        for (Carte i :ca
                ) {
            List<String> lref = i.getReferenti();
            assertEquals( lref.size(),0);
        }
    }

    @Test
    public void test3() throws Exception {

        List<Carte> ca = ctrl.getCartiOrdonateDinAnul( "1948" );
        assertEquals( ca.size(),3 );
        List<Carte> carti = new ArrayList<Carte>();

        carti.add(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        carti.add(Carte.getCarteFromString("Enigma Otiliei;George Calinescu;1948;Litera;enigma,otilia"));
        carti.add(Carte.getCarteFromString("Intampinarea crailor;Mateiu Caragiale;1948;Litera;mateiu,crailor"));

//            for (int j=0;j<ca.size();j++) {
//                    assertEquals(ca.get(j),carti.get(j));
//
//        }
    }

    @Test
    public void test4() throws Exception {
        //P->B->A->C B-valid A-valid C-valid
        List<Carte> ca = ctrl.cautaCarte( "ion" );
        for (Carte i :ca
                ) {
            List<String> lref = i.getReferenti();
            for (int j=0;j<lref.size();j++) {
                if(lref.get( j ).contains( "ion".toLowerCase() )){
                    assertEquals( lref.get( j ).contains( "ion".toLowerCase()) ,true );
                }
            }
        }
        Carte c = new Carte();
        List<String> autori = new ArrayList<String>();
        autori.add( "Irina Binder" );

        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add( "fluturi" );
        cuvinteCheie.add( "iubire" );

        c.setTitlu( "Fluturi" );
        c.setReferenti( autori );
        c.setAnAparitie( "2001" );
        c.setEditura( "Elf" );
        c.setCuvinteCheie( cuvinteCheie );

        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            throw e;
        }
        assertEquals(8, ctrl.getCarti().size());
        assertEquals("Fluturi", c.getTitlu());
        assertEquals(autori,c.getReferenti());
        assertEquals( "2001",c.getAnAparitie() );
        assertEquals("Elf", c.getEditura() );
        assertEquals(cuvinteCheie, c.getCuvinteCheie()  );

        List<Carte> car = ctrl.getCartiOrdonateDinAnul( "1948" );
        assertEquals( car.size(),3 );
        List<Carte> carti = new ArrayList<Carte>();

        carti.add(Carte.getCarteFromString("Dale carnavalului;Caragiale Ion;1948;Litera;caragiale,carnaval"));
        carti.add(Carte.getCarteFromString("Enigma Otiliei;George Calinescu;1948;Litera;enigma,otilia"));
        carti.add(Carte.getCarteFromString("Intampinarea crailor;Mateiu Caragiale;1948;Litera;mateiu,crailor"));
        assertEquals( carti.size(),car.size() );
//            for (int j=0;j<car.size();j++) {
//                    assertEquals(car.get(j),carti.get(j));
//
//        }

    }
}