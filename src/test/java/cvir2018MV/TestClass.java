package cvir2018MV;

import cvir2018MV.control.CarteCtrl;
import cvir2018MV.model.Carte;
import cvir2018MV.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class TestClass {
    private CarteCtrl ctrl;
    private CartiRepoMock ci;

    @Before
    public void init(){
        ci = new CartiRepoMock();//
        ctrl = new CarteCtrl(ci);
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();



    @Test
    public void test1ECP() throws Exception {
        Carte c = new Carte();
        List<String> autori = new ArrayList<String>();
        autori.add( "Mateiu Caragiale" );

        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add( "mateiu" );
        cuvinteCheie.add( "crailor" );

        c.setTitlu( "Intampinarea crailor" );
        c.setReferenti( autori );
        c.setAnAparitie( "1948" );
        c.setEditura( "Litera" );
        c.setCuvinteCheie( cuvinteCheie );

        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            throw e;
        }
        assertEquals(8, ctrl.getCarti().size());
        assertEquals("Intampinarea crailor", c.getTitlu());
        assertEquals(autori,c.getReferenti());
        assertEquals( "1948",c.getAnAparitie() );
        assertEquals("Litera", c.getEditura() );
        assertEquals(cuvinteCheie, c.getCuvinteCheie()  );
    }

    @Test
    public void test2() throws Exception {

        List<String> autori = new ArrayList<String>();
        autori.add( "Mateiu Caragiale" );

        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add( "mateiu" );
        cuvinteCheie.add( "crailor" );
        Carte c = new Carte( "",autori,"1999","Corint",cuvinteCheie);
        try {
            ctrl.adaugaCarte(c);


//            fail("expected IllegalArgumentException");

        } catch (Exception e) {
            // TODO Auto-generated catch block
           e.getMessage();
        }
        ;
    }


    @Test
    public void test3() throws Exception {
        List<String> autori = new ArrayList<String>();
        autori.add( "Mateiu Caragiale" );

        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add( "mateiu" );
        cuvinteCheie.add( "crailor" );
        Carte c = new Carte( "Enigma23", autori, "1999", "Corint", cuvinteCheie );
        try {
            ctrl.adaugaCarte( c );
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.getMessage();
        }
    }

    @Test
    public void test4 () throws Exception {
        List<String> autori = new ArrayList<String>();
        autori.add( "" );

        List<String> cuvinteCheie = new ArrayList<String>();
        assertTrue( cuvinteCheie.add( "fluturi" ) );
        Carte c = new Carte( "Fluturi", autori, "2010", "Litera", cuvinteCheie );
        try {
            ctrl.adaugaCarte( c );
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.getMessage();
        }
    }



    @Test
    public void test5NECP() {
        List<String> autori = new ArrayList<String>();
        autori.add( "Aa2" );

        List<String> cuvinteCheie = new ArrayList<String>();
        assertTrue( cuvinteCheie.add( "fluturi" ) );
        Carte c = new Carte( "Fluturi", autori, "2010", "Litera", cuvinteCheie );
        try {
            ctrl.adaugaCarte( c );
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.getMessage();
        }
    }

    @Test
    public void test6(){
        List<String> autori = new ArrayList<String>();
        autori.add( "Irina Binder" );

        List<String> cuvinteCheie = new ArrayList<String>();
        assertTrue( cuvinteCheie.add( "fluturi" ) );
        Carte c = new Carte( "Fluturi", autori, "20a", "Litera", cuvinteCheie );
        try {
            ctrl.adaugaCarte( c );
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.getMessage();
        }
    }



    @Test
    public void test7NBVA() {
        List<String> autori = new ArrayList<String>();
        autori.add( "Irina Binder" );

        List<String> cuvinteCheie = new ArrayList<String>();
        assertTrue( cuvinteCheie.add( "fluturi" ) );
        Carte c = new Carte( "Fluturi", autori, "10000", "Litera", cuvinteCheie );
        try {
            ctrl.adaugaCarte( c );
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.getMessage();
        }
    }

    @Test
    public void test8() {
        List<String> autori = new ArrayList<String>();
        autori.add( "Irina Binder" );

        List<String> cuvinteCheie = new ArrayList<String>();
        assertTrue( cuvinteCheie.add( "" ) );
        Carte c = new Carte( "Fluturi", autori, "1999", "Litera", cuvinteCheie );
        try {
            ctrl.adaugaCarte( c );
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.getMessage();
        }
    }
    @Test
    public void test9(){
        List<String> autori = new ArrayList<String>();
        autori.add( "Irina Binder" );

        List<String> cuvinteCheie = new ArrayList<String>();
        assertTrue( cuvinteCheie.add( "dragoste" ) );
        Carte c = new Carte( "Fluturi", autori, "2009", "21", cuvinteCheie );
        try {
            ctrl.adaugaCarte( c );
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.getMessage();
        }
    }

    @Test
    public void test10() {
        List<String> autori = new ArrayList<String>();
        autori.add( "Mihai Eminescu" );

        List<String> cuvinteCheie = new ArrayList<String>();
        assertTrue( cuvinteCheie.add( "luceafarul" ) );
        Carte c = new Carte( "Poezii", autori, "1978", "", cuvinteCheie );
        try {
            ctrl.adaugaCarte( c );
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.getMessage();
        }
    }

    @Test
    public void test11() throws Exception {
        List<String> autori = new ArrayList<String>();
        autori.add( "Mihai Eminescu" );

        List<String> cuvinteCheie = new ArrayList<String>();
        assertTrue( cuvinteCheie.add( "lucea21farul" ) );
        Carte c = new Carte( "Poezii", autori, "1978", "Corint", cuvinteCheie );
        //expectedEx.expect(java.lang.Exception);
        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.getMessage();

        }

    }

    @Test
    public void test12BVA() throws Exception {
        Carte c = new Carte();
        List<String> autori = new ArrayList<String>();
        autori.add( "Luca Caragiale" );

        List<String> cuvinteCheie = new ArrayList<String>();
        cuvinteCheie.add( "mateiu" );
        cuvinteCheie.add( "crailor" );

        c.setTitlu( "Castelul Huniazilor" );
        c.setReferenti( autori );
        c.setAnAparitie( "1948" );
        c.setEditura( "LiteraRomana" );
        c.setCuvinteCheie( cuvinteCheie );

        try {
            ctrl.adaugaCarte(c);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            throw e;
        }
        assertEquals(8, ctrl.getCarti().size());
        assertEquals("Castelul Huniazilor", c.getTitlu());
        assertEquals(autori,c.getReferenti());
        assertEquals( "1948",c.getAnAparitie() );
        assertEquals("LiteraRomana", c.getEditura() );
        assertEquals(cuvinteCheie, c.getCuvinteCheie()  );
    }


}
