package cvir2018MV.util;

import cvir2018MV.model.Carte;

public class Validator {

	public static boolean isStringOK(String s) throws Exception{
		boolean flag = s.matches("[a-zA-Z]+");
		if(flag == false)
			throw new Exception("String invalid");
		return flag;
	}

	public static void validateCarte(Carte c)throws Exception{
		if(c.getCuvinteCheie()==null){
			throw new Exception("Lista cuvinte cheie vida!");
		}
		if(c.getReferenti()==null){
			throw new Exception("Lista autori vida!");
		}
		if(c.getTitlu().length()<3){
			throw new Exception("Titlu trebuie sa contina minim 3 caractere!");
		}
		if(c.getTitlu().length()>255){
			throw new Exception("Titlu trebuie sa contina maxim 255 de caractere!");
		}
		if(c.getEditura()==null){
			throw new Exception("Editura nu trebuie sa fie vida!");
		}
		if(c.getAnAparitie()==null){
			throw new Exception("Anul este vid!");
		}
		if(c.getAnAparitie().length()!=4){
			throw new Exception("Anul trebuie sa fie din 4 cifre!");
		}
		if(!isOKString(c.getTitlu()))
			throw new Exception("Titlu invalid!");
		if(!isOKString(c.getEditura()))
			throw new Exception("Editura invalida!") ;
		for(String s:c.getReferenti()){
			if(!isOKString(s))
				throw new Exception("Autor invalid!");
		}
		for(String s:c.getCuvinteCheie()){
			if(!isOKString(s))
				throw new Exception("Cuvant cheie invalid!");
		}
		if(!Validator.isNumber(c.getAnAparitie()))
			throw new Exception("An aparitie invalid!");
	}

	public static boolean isNumber(String s){
		return s.matches("[0-9]+");
	}

	public static boolean isOKString(String s){
		String [] t = s.split(" ");
		if(t.length==2){
			boolean ok1 = t[0].matches("[a-zA-Z]+");
			boolean ok2 = t[1].matches("[a-zA-Z]+");
			if(ok1==ok2 && ok1==true){
				return true;
			}
			return false;
		}
		return s.matches("[a-zA-Z]+");
	}
	
}
